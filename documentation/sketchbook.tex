\documentclass{report}
\usepackage{mdwlist}
\usepackage{fancyref}
\usepackage{fontspec}
\defaultfontfeatures{%
	Ligatures=TeX
}

\usepackage{polyglossia}
\usepackage{microtype}
\usepackage{selnolig}

\usepackage[%
	colorlinks=true]{hyperref}
\title{hdrtools -- Sketchbook}
\author{Urs Liska (git@ursliska.de)}
\begin{document}

\maketitle

This document is \emph{not} a manual or any kind of documentation.
It is a sketchbook that I use for keeping track of my ideas on the subject and the potential development project.
But it can also be used to allow others to get an impression of the work.
So everything that follows may or may not be polished, may or may not be structured in a meaningful way.

\tableofcontents

\chapter{Basic Concept}

\section{Two Recording Paradigms}

The paradigm used for recording classical music considerably differs from that of recording pop music.
Of course there are many more styles of music and of course there will be many cases where this assignment isn't right -- for example there seems to be an increasing number of productions in the Jazz or Pop genre that are created using some form of the other approach.
But I think there basically are two main ways of approaching a studio recording.

The \emph{pop} approach is built on top of the model of a multi-track magnetic tape.
One can listen to what has already been recorded and play along, recording additional tracks all the time.
So any \emph{take} will be recorded at its definitive moment on the \emph{timeline}.
If one doesn't want to use that take one records an \emph{overdub} on a new track and decides during the mixdown which take will finally be heard.
The different parts (voices/instruments) of the arrangement can be recorded one after another (which for example allows one to have completely different acoustics for the tracks).

The \emph{classical} approach rather imitates a stereo magnetic tape treated with scissors.
Everything that is heard simultaneously is recorded at the same time (“live”), so each take contains the complete arrangement.
Multiple takes of the same parts of the music can be compared afterwards and combined to a seemingly continuous recording.
The editor selects and “cuts” \emph{regions} from the original takes and arranges them in the right order, glueing them together with short \emph{cross-fades} (imitating diagonal cuts on the magnetic tape of former times).

Most audio recording software (at least I don't know of any exception) or \textsc{daw} (Digital Audio Workstation) is using a design that directly reflects the \emph{pop} approach outlined above.
Examples of such programs are Ardour (as the major open source candidate), ProTools, Sequoia/Samplitude or Pyramix.
The heart of these programs is an \emph{Arranger} window exposing a horizontal \emph{time line} representing the flow of time and a vertical stack of \emph{tracks} representing parallel layers of music.
While this also works quite well for recordings of classical music -- and in fact is used like this in studios around the world -- I don't think this is a \emph{natural} design for this kind of work.

\section{Recording/Editing “Classical Music” with Current Software}

With a clasical music style recording we -- conceptually -- have only one track at a time.
This track represents the whole arrangement -- the mixdown of all involved sound sources (mainly microphones).
In fact we are recording on multiple tracks (each sound source has its own track) -- but this isn't conceptually a part of the arrangement, but only a means of having the sound sources discrete up to the final mixdown.
Sometimes it can actually be useful to access the individual tracks during the editing process, but only for very rare occasions when a difficult acoustic situation makes it necessary to fine-tune the exact position of a cut or a cross-fade individually for the different sound sources.
Usually we work on the sum of the tracks as a single object, and the software has to provide means of “artificially” keeping the parallel regions glued together.

The editor usually uses additional tracks as a kind of desktop or playground to move around alternative takes during the process of deciding which take he uses for which part of the music.
But while this works I am convinced that this isn't an ideal approach for managing the complexity of a classical recording.
One of the major tasks in editing a classical recording is to keep track of all the takes that one has recorded, their good and weak parts --  and finally which take to use for the specific section of the music one is currently working at.
If the recording is complicated and there are many takes around and/or there have to be many edits this approach soon gets quite complex and sometimes even frustrating, (practically) no matter how good the original documentation of the recording session is.
Therefore I had the impression that there must be an approach suited better for this specific kind of work.
Later I will lay out a concept of how this might be designed.

\chapter{Overall Structure of the Project}

\section{Used Programming Tools}
If I really start working on this project it probably will be using Python and PyQt

\section{Layout of the Project}
The project consists of several subprojects that are partly individual, partly interdependent.
Deciding about this issue will be an important part of the design of the project, because otherwise I risk multiple “restarts”.

The ultimate goal is one GUI application that one can use for major parts of the production process.
As this will \emph{use} everything else it will inherently be the last item to do.
But everything attacked before must take this ultimate goal into account for its design.
But the development of the GUI app doesn't have to wait until everything else is finished (which it probably won't ever be anyway).
If it is based on good design decisions one could well start with a partially working GUI app.
For example one can have an app for tagging takes without the ability to record takes (but only to import them) and to export a region list.
Or one can have a GUI for \emph{using} the tagged takes (i.e. the central task of the editing process) while still having to enter the tags with CLI scripts.

\bigskip

The individual “bricks” of the toolset should form a library in a subfolder, exposing a clean API.
This API will eventually be used by the GUI application, but additionally by a set of scripts that are available as the command line interface to the end user.
An example for such a script could apply a tag to a take or return all takes that have a certain rating for a given moment of the piece.
(Or more concretely: “Show me all takes that have a rating of 4 or better for measure 43”)

\chapter{Ideas on Individual Topics/Tasks}

\section{Storage and Versioning}

The projects in the final GUI application (or should this be approached already at a lower level?) should be managed using a Git repository as this provides unparalleled control over versioning and undo/redo operations.
For this to work smoothly the storage of information should be as modular as possible.
Instead of one big XML file there should be many small text files (see \fref{sec:file-format}).
For example any single take should have its corresponding file.
Of course this has to be designed very carefully in order to avoid the potential for merge conflicts as far as possible.
And at least make sure that any possible merge conflicts can be solved with the aid of the program (should be possible because the nature of the potential merge conflicts should be quite predictable).

I see several advantages using Git in the storage concept:
\begin{itemize*}
\item Undo/redo cannot only be \emph{unlimited} but also completely \emph{selective}.\\
That is one can undo any single earlier operation (as long as it doesn't conceptually interfere with other operations).
\item There is an exhaustive \emph{project history} readily available that can be presented in any desired form. 
For example it would be easy to display all changes done to a specific take.
Or to show the history of changes to the same element, e.g. all changes to the comment on a specific take
\item Branching provides a way to always work in the context of an implicit session.
There is no need for “autosave”, because all changes are stored immediately to disk, so a program crash doesn't lose any information.
Instead of “saving” at the end of the session the current branch is merged into master.
\item I'm sure one can make good use of branching to design work-flows with named sessions (e.g. for trying out some hacks or to switch working context temporarily).
But I will have to think more about this --  as it is a concept not usually incorporated in DAW solutions it should be “brilliantly” devised ;-)
\end{itemize*}

However, this has to be absolutely transparent.
The ordinary user should not be required to learn \emph{any} Git (except for the concepts that are directly used for the app) -- because having to learn such a totally unrelated software would be an massive obstacle getting acceptance for the new concept.

\subsection{Collaboration}
And of course it will be a vast enhancement if one can collaborate on a project.
Git allows very smooth collaboration by several people working on the same files.
The risk of running into conflicts is rather small, with the data stored in a large number of little files it is even smaller.
And as the form of the contents of the files is well known, possible merge conflicts should be quite predictable.
So if for example two people modify the same item (e.g. the rating for a certain take) it should be fairly straightforward to prompt the user with a message like: “Anna rated this take with "excellent", you with "unusable". What should we do?”
But if for example Anna updated the rating while Peter added several position markers there won't be any conflict at all!
While the editor is working on the editing, a musisician (for example) can work on tagging takes.
Or all musicians share the work of originally tagging the takes.
Or, when checking the first edit, a musician can make his suggestions directly within the project.\\
For this to work I have to think about the option of setting up the project as a shared repository -- with the audio files being physically available at each machine, which should transparently work because they reside in a directory that is referenced by a link anyway.
As the audio files are static and won't be changed at all they can safely be included in the Git repository, but Git-annex could be an option here.

\subsection{File Format}
\label{sec:file-format}
As mentioned earlier I don't want one big XML file, but a bunch of smaller ones.
While thinking about how to structure these I thought of the syntax of LilyPond%
\footnote{\url{http://www.lilypond.org}} 
input files with their nested curly brackets.
This brought me to think about using actual LilyPond files as the structure for storing information, as they can hold actual information on the musical structure.
And LilyPond can be used for visualizing results!
At least for outputting an editing plan, as a vision even as an overlay to the scanned score.

Each “take” will be represented by one .ly file, with one music expression for each sub-take.
The music expression exactly spans the usable area of the (sub-)take, possibly involving \textbackslash partial.

There should be one .ly file representing the overall structure of the piece.
However I should take care that it is very easy to get the file up to start working on the content.
It is no option to be forced to enter the whole structure of the piece first.
One should start with just supplying the overall number of measures (maybe with an additional step for repeats).
These measures should be interpreted without any time signature at first.
Only along the way this structure could be refined.
For example if I enter a comment that refers to a specific point in the measure (e.g. “this take is usable from the last quaver in m. 32”) This information should be entered as a concrete LilyPond syntax (but of course a GUI should wrap that up graphically).
Of course in traditional pieces with no or few time signature changes one could easily enter them in a dedicated widget

The .ly files should usually \emph{not} be edited manually but by a script.
This script has to ensure that all files of the project are kept in sync, for example by propagating new information about the formal structure of the piece to all affected files.

The layout of the .ly files should be very line-by-line, i.\,e.\ have as little information as possible in one line, to allow for easy parsing and efficient diffing.
And as the files are intended to be machine-read it doesn't matter if one has to scroll a lot.

There will be the need for many new commands, e.\,g.\ Scheme functions.
For example each subtake will start and end with a \textbackslash timecode entry that defines the exact segment from the audio file.
Later one can gradually refine that structure by adding additional timecode entries (UI should be similar to setting markers in traditional DAWs).
There could be a function (called implicitely upon certain events) that takes timecode tags from other takes and enters “guessed timecode” entries.
This is intended to accomodate greatly varying measure lengths.

Considering how to structure the file layout for rating takes:
One could either enter marker commands directly in the text or create nested music expressions.
The last one is probably better.
The UI should define a range in musical terms as well as the timecode.
Then in the file the overall music expression is split, a curly bracket entered, followed by a timecode marker (the corresponding at the end of the range).
If a new range is inserted \emph{inside} an existing one it isn't nested but rather split in three independent subsections.
If I add a range that partly overlays an existing subsection (M. 12--15 had been labeled “A”, now I label 14--20 “B” it should create subsections 12--13 and 14--20).
In addition to these subsections one can attach non-range markers to specific points in time (“wrong pitch at 4th quaver in m.23”).
Question is whether this should be implemented as a single command or implicitely as a short subsection.

\section{Intended Work-Flow of the Recording Process}

I outline the process of creating a classical recording and give some thoughts on the distribution of tools needed for that.

\subsection{Setting up the recording}
This isn't actually the heart of the efforts. But of course it should be integrated in the GUI.
As most people probably prefer setting up their recording environment with a GUI it would be preferable to find a way to \emph{use} existing software.
Maybe one could use something to set up and meter via ALSA or Jack?

\subsection{Recording Takes}
As a start it is acceptable to only offer an opportunity to import existing takes. 
But on the long run it is indispensable to be able to record from within the application.
This is also necessary because one should definitely be able to start tagging the takes while (or immediately after) recording them.

Minmal requirement for recording is the automatic naming of the recorded files.
And it should be possible to provide a stereo file afterwards (as the followings steps are done using only the stereo mix).
There could either be original tracks marked as 'stereo mix' or such a mixed file will be created automatically (either by creating some kind of 'bus' during the recording or by triggering a mixdown after the recording is complete).

\subsection{Remote Control}
It is desirable to be able to remotely control at least parts of the process.
The very least would be able to control the recording process (i.e. start recording, automatic naming of takes, optionally basic tagging, playback) -- anything that is necessary to control the recording directly from the musicians POV.
This would probably be implemented using OSC, transferred through WiFi or possibly Bluetooth.

The idea of this is enabling a musician to control a recording session on his own (with the noisy gear located outside the recording room), controlling the process with an (e.g.) Android App.

\subsection{Analyzing/Managing Takes}
This is the central issue of the whole project.
Therefore there will be a dedicated section on this topic.

\subsection{Preparation and Export of the Editing Plan}
The management of takes should result in a usable editing plan.
The program should be able to export some kind of “region list” that can be used to finish the project in other dedicated audio software.
The GUI app should possibly allow for playing this list back (i.e. all selected snippets one after another).
It would be nice to have one complete region list with the option to give alternative takes for certain parts (that would be placed on additional tracks in the usual \emph{Arranger window} concept.

This editing plan should at least be exported in a well-structured readable form to be used while compiling in another program.
(Optionally) export the plan to Markdown, PDF (through \LaTeX), plain text or .rtf.
But it would be even better if one could export a project file for other programs that can instantly be loaded. 
It should at least be possible to do that for Ardour.
The other program would then start its project with\\
-- all takes available in a region list\\
-- mixer set up according to the projects mixer set-up\\
-- the regions in our editing plan laid out in the right order in the Arranger window.

\subsection{Digital Editing, Mixing and Mastering}
It would definitely be way beyond the scope of this project to offer these tools.
At least we won't provide anything near professional quality.
The actual editing, final mixing and mastering should really be done using professional tools.

But on the other hand it is essential for an editor to at least give edits a quick shot in order to decide whether his selection is realistic.
I think it is possible to create (GUI) tools to make quick edits (i.e. trim two takes and overlay them with a cross-fade).
Probably it is possible to have a GUI to create such pairs of takes, and organize them to a kind of sketchy first edit -- resulting in a complete audio file of the whole piece.
But more thoughts on this later in the chapter on GUI issues.

In order to make it as usable as possible (and to ease acceptance) it would definitely be preferable if we could export a region list in other software's format


\section{Project Hierarchy}
\subsection{Project}
The highest level organisational element is a \emph{project}.
This is represented by one named directory.
It contains several subdirectories, one of them being a link to an external directory where the actual audio files are stored.
The audio files are never stored within the project directory, and they aren't managed by the Git repository either.

IDEA: Check out git-annex if that's of any help here.
But maybe it's better to have a link to a static dir anyway.


\subsection{Project/Parts}
A Project contains one or more \emph{parts}.
Parts correspond to songs, movements or the like.
Each part keeps information on the musical structure (e.g. number of measures etc.)---probably in a LilyPond source file.
We absolutely want to know where we are in the piece, we want to be able to play parts of audio files that represent known parts of the music.

\subsection{Part/Score}
Each part can have a score associated with it.
This is a pdf file that should at least be tagged with a bar number per page.
It should be easy to tag the individual systems with bar numbers too.
Then we can have a pane showing the score linked to the cursor of the current song position.

\subsection{Part/Takes}
A part contains zero, one or more \emph{takes}.
A take corresponds to a recorded audio file (i.e.\ a complete recorded take, regardless of its actual content).
Actually an audio file can correspond to a group of files (coresponding to the tracks of the multi-track set-up), but in musical terms and such of the time-line these represent one recording.
So the software has to transparently keep track of these groups of files (probably through naming conventions).
One \emph{take} represents one recorded timespan, transparently containing all parallel tracks.

If audiofiles are recorded they are directly assigned to the active Part.
However I'm not completely sure what to do with importing files.
I have to think about this quite extensively:
\begin{itemize*}
\item Will they be imported in a Part or can they also be imported in a project?
\item Is there a need to keep track of all audiofiles in the whole project?
\item What about naming conventions (files to be imported will often not adhere to the same naming conventions)?\\
\emph{Edit:} I think \emph{any} reasonable naming scheme of a professional software could be matched and mapped with regular expressions.
(Maybe create default mappings for major programs and design a visual editor for non-default mappings.)
So we don't have to rename the included files but create the right mapping between file name and internal naming convention (I think: in the “take” file).
This way we can simply include an existing recording session with a symlink, without interfering at all with that (might be a session from another program).
\item How to deal with inconsistent set-ups\\
(inconsistent audio formats, inconsistent multi-track set-up)?
\end{itemize*}

\subsection{Takes/Subtakes}
A take contains one or more \emph{subtakes}.
A subtake is one recorded musical unit.
If the musicians stop playing and do the same (or something different) again while the recording is still active we have two subtakes.

In order to be “backward-compatible” with usual conventions how the scores are annotated, one could automatically name takes like “001”, “002” etc. if there is only one take within an audiofile, and use “003-01”, “003-02” if there are more than one.
On the other hand this might cause problems if I add a second take to an audiofile after I have already done any work on that take with its original naming.
A solution to that problem might be to start naming the takes “001-01” internally but omit the suffix for display if there is only one take in the audiofile.\\
Anyway, this issue needs further consideration \emph{before any implementation}.

\subsection{Subtake}
A \emph{subtake} is the lowest-level unit of a recording session.
It represents a musically continuous recorded part.
By default one subtake is contained in one audiofile, but there also can be more than one subtake in an audiofile if the recording supervisor didn't turn off the recording while the musicians did several attempts.

In our software a “take” will by default contain one “subtake” of the same length.
But the editor can at first “trim” the take to the length of its actual recorded content.
Then the editor can define several independent subtakes within the take, again trimming them to leave out the empty time before or after the music.
As the end user is generally confronted with “subtakes” and not “takes” this means that this “empty” time is practically removed from the project and won't be seen again.
It should be also possible to mark a whole take as “invalid” or “unusable”, also practically making it invisible to the editor.
Of course there should be an option or a list “invalid takes” that one can inspect if necessary (for example to reconsider all decisions).

\section{Tagging Takes}
The ability to comprehensively “tag” recorded takes, manage and save this information and use it to create an efficient work-flow in the process of finding an editing plan is the key feature of this project, so it is crucial to design this as thoroughly as possible.
In fact the structure of information should be the first issue to tackle.

\bigskip

Maybe the single most ubiquituous question we're faced while preparing an editing plan is which take(s) we can use for any given part of the piece at hand.
So it seems natural to think about tools that can aid us with this task.

In a usual constellation we have audio files that are consecutively numbered and references to these “takes” in the score.
If we have been accurate during the recording session we have marks for the beginnings of all takes and probably also of the ends.
Probably we have some extra marks about problematic or positive spots in certain takes, and we may even have some comments about some of the takes we have already selected for use in the edited file.
But if we are faced the above question, if we need (e.g.) alternative takes for measure 112, we don't immediately see from the score which takes we should further investigate.
Basically we have to go back in the score and “collect” all beginnings of takes to identify the ones that contain m. 112.
We have to ignore all takes that end before m. 112.
And to be really sure we don't omit any takes we would really have to go back to m. 1.
Well, often we don't have to do this really systematically because we for example recall that we worked on m. 20--50 in takes 3--12, m. 45--70 in takes 13--21 etc.
But sometimes we can't be really sure about this (maybe we did a few takes with m. 105--115 in between some other work?), and also, if it has been a complicated session, this process can become quite messy.

\medskip
Now imagine a situation where the takes know themselves about their contents.
Of course we have collected this information ourselves, but I'm talking about not much more than taking notes while listening to the takes (which is something we have to do anyway).
The basic information we would have in all takes is their actual starting and ending point (I'd say: the points enclosing the actually \emph{usable} content).
We would then ask the computer about m. 112 and would be confronted with a list of all takes containing m. 112!

If we go one step further we could tag the takes and rate them according to their musical quality: Our result list can now be ordered according to the usability of the takes.

And one more step is to split the take into an arbitrary number of ranges

\section{Ideas on the User Interface}
\begin{itemize}
\item Display \emph{takes} as rectangles with different sections colored according to their rating.
Depending on the current “view” and zoom there could be waveforms (or volume graphs) be layered.
\item Have two main views: an “arranger“ and a “take edit” view.
\item The “arranger” view is centered around a cursor representing a place in the timeline.
This of course can be zoomed to any level of detail.
Two top “tracks” (???) contain an editing plan (i.\,e. all takes marked as “use this”) and a “staging area” (all takes marked as “can be used”).
Both tracks have a way to display overlaps.

Show a stack of takes that contain this marker position, sorted by rating or take number.
The boxes for the takes have to be flexibly stretched to match the timeline of the project
For this the takes should store the necessary information themselves (i.e. a list of reference points and measure numbers, from which the stretching factor for any given section can be calculated).
This way one has an immediate overview of possible takes for the current position, with visual feedback on the length of useable parts.

This arranger view is used for actually creating the editing plan.
It relies on all takes being \emph{at least} tagged with their content.
There should be a separate window (dock?) containing a list with all untagged takes.

Double clicking on a take should open its editing (i.e. tagging) window.
\item It really is a question if this arranger view should enable at least basic editing of the cuts (i.e. trim takes and edit crossfades) to be able to have an audible sketch.

I think this is an idea for a v. 2.0 but should be taken into account for the design right from the start.

Any information about position of takes on the timeline should \emph{not} be stored as timestamps, but in terms of musical moments (i.e. bar numbers) and/or relatively to the preceding region.
This is the way to go to have later regions move automatically when earlier regions change (one of the major hassles when working with traditional DAWs).
Concretely a region should store its length and the length of its cross-fades, then the next region can calculate its starting point.
For playback this could even be done “live“, i.e. whenever a region starts playing it calculates when the next region should be starting.

But there is the possibility of gaps (when the plan isn't completed yet).
Maybe for playback this should just result in a default fade-out (say 250ms), a short pause, and a default fade-in.

\item Have an optional “score pane” that shows the score.
Use scanned material (in PDF, provide tools to convert images to pdf if necessary).
Tag with bar number per page or even per line (just click below a staff and enter a bar number).
Then the view is linked to the current cursor position.

Scores should be stored separately (like audio files), but the links and tags are versioned.
\item If it is possible to use LilyPond as the file format it would be terrific to overlay a LilyPond score over this score, “printing” the editing plan directly on top of the scanned score.
\end{itemize}


\end{document}