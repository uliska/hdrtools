hdrtools
========

hdr may once become a collection of tools designed for dealing with audio recordings with the "classical music" approach on a professional level.  
It aims at providing a work-flow that is different from the usual timeline/arranger window paradigm.

With "classical music approach" I mean the concept of recording takes that are then edited and "glued" together horizontally, without using overdubs.

The toolset cover the steps of recording the takes and then managing them up to the finished editing plan, but not the actual editing or mixing/mastering.  
So it does _not_ intend to compete with DAW software like Ardour, Sequoia/Samplitude, ProTools or whatever, but rather sees itself as a platform to prepare a project up to the stage where the other mentioned programs will be used.

For more thoughts on the concept please read the manual/sketchbook.


"State of Development"
----------------------

So far this all is only an idea, and I use this repository only to collect my thoughts and ideas.

The programming platform will probably be Python and PyQt, so it should yield a cross-platform result, although my main OS is Linux.

The idea is to develop a set of CLI scripts that could also be used independently (or included in other programs) and later combine them to a main GUI application.

If some of the ideas attract your attention and you would like to share some thoughts or are even interested in participating don't hesitate to contact me through git@ursliska.de
